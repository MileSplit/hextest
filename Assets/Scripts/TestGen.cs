﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Generates the Board, and all the cells in it.
/// </summary>
public class TestGen : MonoBehaviour {
    public GridPoint[] p1Starting, p2Starting, goldSpots;
    public GameObject hexTest;
    public static TestGen instance;
    public Toggle toggle;
    public Image test;

	// Use this for initialization
	void Start () {
        LeanTween.init(800);
        instance = this;
        for (int y = 0; y < Board.sizeY; y++)
        {
            for (int x = 0; x < Board.sizeX; x++)
            {
                float yDist = Board.yLength * y;
                if (x % 2 == 0) yDist -= Board.yLength / 2;
                if (!(x % 2 == 1 && y == Board.sizeY - 1))
                {
                    GameObject g = Instantiate(hexTest, new Vector3(x * Board.xLength, yDist), Quaternion.identity) as GameObject;
                    Cell cell = g.GetComponent<Cell>();
                    cell.Init(x, y);
                }
            }
        }

        //Test
        Invoke("SpawnIn", 0.7f);

        

    }

    public void Reload()
    {
        Board.ClearBoard();
        Application.LoadLevel(Application.loadedLevel);
    }

    public void NextTurn()
    {
        Board.NextTurn();
    }

    public void SpawnIn()
    {
        foreach (GridPoint p in p1Starting)
        {
            Cell c = Board.GetCell(p);
            c.Alive = true;
            c.Team = Team.P1;
        }

        foreach (GridPoint p in p2Starting)
        {
            Cell c = Board.GetCell(p);
            c.Alive = true;
            c.Team = Team.P2;
        }
        foreach (GridPoint p in goldSpots)
        {
            Cell c = Board.GetCell(p);
            c.GoldSpot = true;
        }
    }
}

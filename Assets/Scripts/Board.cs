﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public delegate void NextTurnHandler();
public delegate void ActionPerformedHandler();
/// <summary>
/// The board class represents the physical board, and contains methods and 
/// variables to change and acess data from the board.
/// </summary>
public class Board : MonoBehaviour {
    //----------------------------------------------------------------------
    public readonly static int sizeX = 7, sizeY = 8, minN = 2, maxN = 2;
    public readonly static float xLength = 0.75f, yLength = (0.86602540378f);
    public readonly static Color P1Color = Color.red, P2Color = Color.blue;
    //-----------------------------------------------------------------------
    /// <summary>
    /// DO NOT USE
    /// A collection of all cells filled by init
    /// </summary>
    static Cell[,] cellLocs = new Cell[sizeX,sizeY];
    /// <summary>
    /// Instad of using this, please consider using the GetTeam() method.
    /// </summary>
    public static List<Cell> cellList = new List<Cell>();
    //-----------------------------------------------------------------------
    public static Team whosTurn = Team.P1;
    public static int maxActions = 1;
    public static int actionsThisTurn = 0;
    public static event NextTurnHandler OnNextTurn;
    public static event ActionPerformedHandler OnActionPerformed;
    //-----------------------------------------------------------------------

    /// <summary>
    /// Moves the cell to the all cells array.
    /// </summary>
    public static void AssignCell(Cell cell)
    {
        cellLocs[cell.point.x, cell.point.y] = cell;
        cellList.Add(cell);
    }

    /// <summary>
    /// Performs one round of life and death on one team
    /// </summary>
    public static void GolTeam(Team team)
    {
        List<Cell> cells = GetTeam(team);
        cells.AddRange(GetTeam(Team.Unclaimed));
        //FIrst go and mark the unworthy
        foreach (Cell c in cells)
        {
            int n = c.CountNeighbors(team);
            if (n >= minN && n <= maxN)
            {
                c.futureState.alive = true;
                c.futureState.team = team;
            }
            else
            {
                c.futureState.alive = false;
                c.futureState.team = Team.Unclaimed;
            }
        }
        //Then apply
        foreach (Cell c in cells)
        {
            c.ApplyFutureState();
        }
    }

    /// <summary>
    /// Gets the cell at the specified point Will throw CellNotFound Exception.
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>
    public static Cell GetCell(GridPoint point)
    {
        try
        {
            Cell c = cellLocs[point.x, point.y];
            if (c != null) return c;
            else throw new CellNotFound("Cell Was not found.");
        }
        catch
        {
            throw new CellNotFound("Cell was not found");
        }
    }

    /// <summary>
    /// Gives a list of all cells from a team.
    /// </summary>
    /// <param name="team"></param>
    /// <returns></returns>
    public static List<Cell> GetTeam(Team team)
    {
        return cellList.Where(c => c.Team == team).ToList();
    }

    /// <summary>
    /// Moves to the next turn.
    /// 1) Apply GOL
    /// 2) Switch Turn to other player.
    /// </summary>
    public static void NextTurn()
    {
        GolTeam(whosTurn);
        whosTurn = (whosTurn == Team.P1) ? Team.P2 : Team.P1; //Switch Teams.
        actionsThisTurn = 0;//Calculate Actions
        //Max actions
        #region
        //max actions
        int counter = 1;
        foreach (GridPoint g in TestGen.instance.goldSpots)
        {
            if (GetCell(g).Team == Board.whosTurn)
            {
                counter++;
            }
        }
        maxActions = counter;
        #endregion//Calculate max actions
        if (OnNextTurn != null)
        {
            print("Turn Ended");
            OnNextTurn.Invoke();
        }
    }

    /// <summary>
    /// Used to invoke the event that an action was performed.
    /// </summary>
    public static void CallAction()
    {
        if (Board.OnActionPerformed != null)
        {
            OnActionPerformed.Invoke();
        }
    }

    /// <summary>
    /// CALL THIS BEFORE LEAVING LEVEL
    /// </summary>
    public static void ClearBoard()
    {
        cellList = new List<Cell>();
        cellLocs = new Cell[sizeX, sizeY];
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Represents a point on the grid using conventional coordinate(x,y).
/// Also contains methods to convert to other coordinate systems (x,y,z)
/// </summary>
[System.Serializable]
public struct GridPoint
{
    public int x;
    public int y;

    public GridPoint(int x, int y)
    {
        if (x >= 0 && x < Board.sizeX && y >= 0 && y < Board.sizeY)
        {
            this.x = x;
            this.y = y;
        }
        else
            throw new PointOutOfRange(string.Format("Point ({0},{1}) is out of range 0-{2}.", x, y, Board.sizeX));
    }

    public GridPoint(int x, int y, bool containsNegitive)
    {
        if ((x >= 0 && x < Board.sizeX && y >= 0 && y < Board.sizeY) || containsNegitive)
        {
            this.x = x;
            this.y = y;
        }
        else
            throw new PointOutOfRange(string.Format("Point ({0},{1}) is out of range 0-{2}.", x, y, Board.sizeX));
    }

    public static GridPoint operator +(GridPoint c1, GridPoint c2)
    {
        GridPoint p = new GridPoint(c1.x + c2.x, c1.y + c2.y);
        return p;
    }

    public static GridPoint operator -(GridPoint c1, GridPoint c2)
    {
        return new GridPoint(c1.x - c2.x, c1.y - c2.y);
    }

    /// <summary>
    /// Converts to cube cords, which makes doing math on points a little easier.
    /// http://www.redblobgames.com/grids/hexagons/
    /// </summary>
    /// <returns></returns>
    public CubeCord toCubeCord()
    {
        int tempX = x,
            tempY = y - (x - (x & 1)) / 2,
            tempZ = -tempX - tempY;
        return new CubeCord(tempX, tempZ, tempY);
    }

    public override string ToString()
    {
        return "(" + x + "," + y + ")";
    }
}

public class PointOutOfRange : Exception
{
    public PointOutOfRange( string message) : base(message)
    {
    }
}

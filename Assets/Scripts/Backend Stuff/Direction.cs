﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction
{
    N,NE,SE,S,SW,NW
};

public enum Team
{
    Unclaimed,
    P1,
    P2
};

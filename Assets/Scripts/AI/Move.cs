﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Represents a turn in the game
/// </summary>
public struct Turn {
    public List<Move> moves;
    public Turn(List<Move> moves)
    {
        this.moves = moves;
    }

    public override string ToString()
    {
        string result = "{";
        foreach (Move m in moves)
        {
            result += m.ToString();
            result += " - ";
        }
        return result + "}";
    }
}
/// <summary>
/// Represents a Move in the game.
/// </summary>
public struct Move
{
    public GridPoint position;
    public MoveType type;

    public Move(GridPoint position, MoveType type)
    {
        this.position = position;
        this.type = type;
    }

    public override string ToString()
    {
        return position.ToString();
    }
}

public enum MoveType
{
    Attack,Create
};

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// A coordinate system similar to (x,y)
/// instead its x,y,z and its based off of an isometric cube.
/// </summary>
public struct CubeCord {
    public int x, y, z;

    public CubeCord(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /// <summary>
    /// Returns the number of "steps" between the two.
    /// </summary>
    /// <param name="other"></param>
    /// <returns></returns>
    public int DistInSteps(CubeCord other)
    {
        return (Mathf.Abs(x - other.x) + Mathf.Abs(y - other.y) + Mathf.Abs(z - other.z)) / 2;
    }

    public static int StepsBetween(CubeCord a, CubeCord b)
    {
        return (Mathf.Max(Mathf.Abs(a.x - b.x), Mathf.Abs(a.y - b.y), Mathf.Abs(a.z - b.z)));
    }

    public override string ToString()
    {
        return string.Format("(x={0}, y={1}, z={2})", x, y, z);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
/// <summary>
/// Very similar to the board class, this class is used in AI calculations
/// to simulate a virtual board to help make decisions.
/// </summary>
[System.Serializable]
public class VirtualBoard {
    VirtualCell[,] cellLocs = new VirtualCell[Board.sizeX, Board.sizeY];
    /// <summary>
    /// Instead of using this, please consider using the GetTeam() method.
    /// </summary>
    List<VirtualCell> cellList = new List<VirtualCell>();


    /// <summary>
    /// This creates a virtual board from the current board
    /// </summary>
    /// <returns></returns>
    public static VirtualBoard CreateFromRealBoard()
    {
        VirtualBoard result = new VirtualBoard();
        foreach (Cell cell in Board.cellList)
        {
            VirtualCell mimic = new VirtualCell(cell.point, cell.Alive, cell.Team, cell.GoldSpot, result);
            result.cellList.Add(mimic);
            result.cellLocs[mimic.point.x, mimic.point.y] = mimic;
        }
        return result;
    }

    /// <summary>
    /// Performs one round of life and death on one team
    /// </summary>
    public void GolTeam(Team team)
    {
        List<VirtualCell> cells = GetTeam(team);
        cells.AddRange(GetTeam(Team.Unclaimed));
        //FIrst go and mark the unworthy
        foreach (VirtualCell c in cells)
        {
            int n = c.CountNeighbors(team);
            if (n >= Board.minN && n <= Board.maxN)
            {
                c.futureState.alive = true;
                c.futureState.team = team;
            }
            else
            {
                c.futureState.alive = false;
                c.futureState.team = Team.Unclaimed;
            }
        }
        //Then apply
        foreach (VirtualCell c in cells)
        {
            c.ApplyFutureState();
        }
    }

    /// <summary>
    /// Gives a list of all cells from a team.
    /// </summary>
    /// <param name="team"></param>
    /// <returns></returns>
    public List<VirtualCell> GetTeam(Team team)
    {
        return cellList.Where(c => c.team == team).ToList();
    }

    /// <summary>
    /// Returns a list of all gold spots, possible performance increase with indexing?
    /// </summary>
    /// <returns></returns>
    public List<VirtualCell> GetGolds()
    {
        return cellList.Where(x => x.goldSpot).ToList();
    }

    /// <summary>
    /// Gets the cell at the specified point Will throw CellNotFound Exception.
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>
    public VirtualCell GetCell(GridPoint point)
    {
        try
        {
            VirtualCell c = cellLocs[point.x, point.y];
            if (c != null) return c;
            else throw new CellNotFound("Cell Was not found.");
        }
        catch
        {
            throw new CellNotFound("Cell was not found");
        }
    }

    /// <summary>
    /// Creates a board based on a move(s) from the player
    /// </summary>
    public void ExeTurn(Turn turn)
    {
        foreach (Move move in turn.moves)
        {
            GetCell(move.position).ForceClick(Team.P2);
        }
    }

    /// <summary>
    /// Returns an array of all empty non gold tiles within (2) from a P2 tile.
    /// </summary>
    /// <returns></returns>
    public List<Move> GetSensiableMoves()
    {
        List<Move> result = new List<Move>();
        foreach (VirtualCell cell in GetTeam(Team.Unclaimed))
        {
            if (!cell.goldSpot && cell.DistenceToNearestAlive() <= 2)
            {
                result.Add(new Move(cell.point,MoveType.Create));
            }
        }
        //Add in attacking the other player
        foreach (VirtualCell cell in GetTeam(Team.P1))
        {
            if (!cell.goldSpot)
                result.Add(new Move(cell.point, MoveType.Attack));
        }
        return result;
    }

    public int GetTeamGoldCount(Team team)
    {
        int counter = 0;
        foreach (VirtualCell cell in GetGolds())
        {
            if (cell.team == team) counter++;
        }
        return counter;
    }

    public override string ToString()
    {
        string result = "";
        for (int y = 0; y < Board.sizeY; y++)
        {
            for (int x = 0; x < Board.sizeX; x++)
            {
                try
                {
                    result += cellLocs[x, y].ToString() + " ";
                }
                catch
                {

                }
            }
            result += '\n';
        }
        return result;
    }
}

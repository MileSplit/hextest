﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using UnityEngine;
/// <summary>
/// The master class that does the logic for the AI player. 
/// Could use some cleaning.
/// Also does timing.
/// </summary>
public class AiPlayer : MonoBehaviour {
    public AiProfile profile;

    private void Start()
    {
        Board_OnNextTurn();
    }
    // Use this for initialization
    void OnEnable()
    {
        Board.OnNextTurn += Board_OnNextTurn;
    }

    void OnDisable()
    {
        Board.OnNextTurn -= Board_OnNextTurn;
    }

    private void Board_OnNextTurn()
    {
        print("Starting logic tree...");
        Invoke("CalculateMove", 0.5f);
    }

    private void CalculateMove()
    {
        if (Board.whosTurn == Team.P2)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            VirtualBoard currentState = VirtualBoard.CreateFromRealBoard();
            //print(currentState.GetTeam(Team.P2).Count);
            //Logic, this can change based on ai prefernces.
            Turn bestTurn = new Turn();
            int bestTurnScore = 0;
            //so we can compare
            int ourCount = currentState.GetTeam(Team.P2).Count,
                thierCount = currentState.GetTeam(Team.P1).Count;
            int movesCount = currentState.GetTeamGoldCount(Team.P2) + 1;
            print("moveCOunt: " + movesCount);



            List<Move> possibleMoves = currentState.GetSensiableMoves();


            var temp = UtilityMethods.Combinations<Move>(possibleMoves, movesCount);


            List<Turn> possibleTurns = UtilityMethods.ConvertToList(temp);

            long prepTime = watch.ElapsedMilliseconds;
            long deepCloning = 0, executing = 0, evaling = 0; 
            //All of this code is very time sensitive
            print("Processing this many moves: " + possibleTurns.Count);
            foreach (Turn turn in possibleTurns)
            {
                var innerWatch = System.Diagnostics.Stopwatch.StartNew();
                innerWatch.Start();
                //See what would change
                //VirtualBoard turnResult = Extensions.DeepClone<VirtualBoard>(currentState);
                VirtualBoard turnResult = VirtualBoard.CreateFromRealBoard();
                deepCloning += innerWatch.ElapsedMilliseconds;
                innerWatch.Stop();
                innerWatch.Reset();
                innerWatch.Start();
                turnResult.ExeTurn(turn);
                turnResult.GolTeam(Team.P2);
                executing += innerWatch.ElapsedMilliseconds;
                innerWatch.Stop();
                innerWatch.Reset();
                innerWatch.Start();
                #region
                //Now evaluate the change
                int currentTurnScore = 0;
                //Stuff well use
                int created = turnResult.GetTeam(Team.P2).Count - ourCount;
                int destroyed = thierCount - turnResult.GetTeam(Team.P1).Count;
                float percentDestroyed = 1 - ((float)turnResult.GetTeam(Team.P1).Count / thierCount);
                float percentCreated = (float)turnResult.GetTeam(Team.P2).Count / ourCount;
                //print(string.Format("pc: {0} = {1} / {2}",percentCreated, turnResult.GetTeam(Team.P2).Count, ourCount));
                if (ourCount < 3)
                {
                    currentTurnScore = (int)(GoldScore(turnResult) * 0.25f + Mathf.Clamp(percentCreated * 2000, 0, int.MaxValue));
                }
                else if (movesCount * 2 + 1 >= thierCount)
                {
                    //Goal is to remove the other player because we have a gold spot.
                    currentTurnScore = (int)(GoldScore(turnResult) * 0.25f + Mathf.Clamp(percentDestroyed * 700f, 0, int.MaxValue));
                    //print(string.Format("finished eval {0}. Move Score:{1} best score:{2}", turn, currentTurnScore, bestTurnScore));
                }
                else
                {

                    currentTurnScore +=
                        (int)(GoldScore(turnResult) * 1f +
                        Mathf.Clamp(percentDestroyed * profile.agressionWeight, 0, int.MaxValue) +
                        Mathf.Clamp(percentCreated * profile.creationWeight, 0, int.MaxValue));
                    //print(string.Format("finished eval {0}. Move Score:{1} best score:{2} PC: {3} PD:{4}", turn, currentTurnScore, bestTurnScore,percentCreated,percentDestroyed));
                }

                if (currentTurnScore > bestTurnScore)
                {
                    bestTurnScore = currentTurnScore;
                    bestTurn = turn;
                }
                #endregion
                evaling += innerWatch.ElapsedMilliseconds;
                innerWatch.Stop();
                innerWatch.Reset();
            }
            long totalTime = deepCloning + executing + evaling;
            print(string.Format("Total Time: {0} deepCloning: {1} exeing: {2} evaling: {3}",
                totalTime,
                (double)deepCloning / (totalTime),
                (double)executing / (totalTime),
                (double)evaling / (totalTime)));

            watch.Stop();
            long totalCompTime = watch.ElapsedMilliseconds;
            OctoGraph.instance.Render((evaling + executing) / (float)totalCompTime);
            print((evaling + executing) + "   " + totalCompTime);
            print((evaling + executing) / (float)totalCompTime);
            AiHand.instance.ExeTurn(bestTurn);
            //Invoke("goToNextTurn",1.5f);
        }
    }

    public string ListToString(List<VirtualCell> input)
    {
        string s = "";
        foreach (VirtualCell m in input)
        {
            s += m.point.ToString();
        }
        return s;
    }

    void goToNextTurn()
    {
        Board.NextTurn();
    }

    public int GoldScore(VirtualBoard board)
    {
        int goldScore = 0;
        foreach (VirtualCell cell in board.GetTeam(Team.P2))
        {
            int dist = cell.DistenceToNearestGold();
            if (dist != 0)
                goldScore += (profile.maxNearGold - dist);
            else
                goldScore += profile.goldValue;
        }
        return goldScore;
    }
}

public static class UtilityMethods
{
    public static IEnumerable<IEnumerable<T>> Combinations<T>(this IEnumerable<T> elements, int k)
    {
        return k == 0 ? new[] { new T[0] } :
          elements.SelectMany((e, i) =>
            elements.Skip(i + 1).Combinations(k - 1).Select(c => (new[] { e }).Concat(c)));
    }

    public static List<Turn> ConvertToList(IEnumerable<IEnumerable<Move>> param)
    {
        List<Turn> inner = new List<Turn>();
        foreach (var e in param.ToList())
        {
            Turn tempTurn = new Turn(new List<Move>());
            foreach (var m in e.ToList())
            {
                tempTurn.moves.Add(m);
            }
            inner.Add(tempTurn);
        }
        return inner;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Like the Virtual Board class, this is very similar to the Cell class.
/// It is used by the AI to help make decisions.
/// </summary>
[System.Serializable]
public class VirtualCell {
    public VirtualBoard parentBoard;
    //Variables...
    public GridPoint point = new GridPoint(0, 0);
    public bool alive = false;
    public Team team = Team.Unclaimed;
    public bool goldSpot;

    /// <summary>
    /// When using life and death this is used as a temp for the data.
    /// </summary>
    public FutureCell futureState;

    public bool isOddRow
    {
        get
        {
            return !(point.x % 2 == 0);
        }
    }

    public VirtualCell(GridPoint point, bool alive, Team team, bool goldSpot, VirtualBoard parentBoard)
    {
        this.point = point;
        this.alive = alive;
        this.team = team;
        this.goldSpot = goldSpot;
        this.parentBoard = parentBoard;
    }

    /// <summary>
    /// Gets the cell in a certain direction.
    /// </summary>
    public VirtualCell GetCell(Direction direction)
    {
        GridPoint change;
        int oddChange = 0;
        if (isOddRow) oddChange = 1;
        switch (direction)
        {
            case Direction.N:
                change = new GridPoint(0, 1, true);
                break;
            case Direction.NE:
                change = new GridPoint(1, 0 + oddChange, true);
                break;
            case Direction.SE:
                change = new GridPoint(1, -1 + oddChange, true);
                break;
            case Direction.S:
                change = new GridPoint(0, -1, true);
                break;
            case Direction.SW:
                change = new GridPoint(-1, -1 + oddChange, true);
                break;
            case Direction.NW:
                change = new GridPoint(-1, 0 + oddChange, true);
                break;
            default:
                change = new GridPoint(0, 0, true);
                break;
        }
        try
        {
            return parentBoard.GetCell(point + change);
        }
        catch (PointOutOfRange e)
        {
            throw new CellNotFound("V-Cell was not found, (Probaly out of range?)");
        }
        catch (NullReferenceException e)
        {
            throw new CellNotFound("V-Cell was not found, (Probaly a odd tile?)");
        }
    }

    public int CountNeighbors(Team team)
    {
        int counter = 0;
        foreach (Direction d in Enum.GetValues(typeof(Direction)))
        {
            try
            {
                VirtualCell c = this.GetCell(d);
                if (c.alive && c.team == team) counter++;
            }
            catch (CellNotFound e)
            {
                //DO Nothing
            }
        }
        return counter;
    }

    /// <summary>
    /// Applys the temp state
    /// </summary>
    public void ApplyFutureState()
    {
        alive = futureState.alive;
        team = futureState.team;
    }

    /// <summary>
    /// Emulates a touch on a specific object, no conditions.
    /// </summary>
    public void ForceClick(Team player)
    {
        alive = !alive;
        team = alive ? player : Team.Unclaimed;
    }

    /// <summary>
    /// returns the "steps" to the nearest living cell of the same team.
    /// </summary>
    /// <returns></returns>
    public int DistenceToNearestAlive()
    {
        int best = int.MaxValue;
        foreach (VirtualCell cell in parentBoard.GetTeam(Team.P2))
        {
            int v = CubeCord.StepsBetween(cell.point.toCubeCord(), point.toCubeCord());
            if (v < best)
            {
                best = v;
            }
            //Debug.Log("Dist from " + point.ToString() + " to " + cell.point.ToString() + " = " + v + "\n" + point.toCubeCord().ToString() + " to " + cell.point.toCubeCord().ToString());
        }
        if (best == int.MaxValue) throw new CellNotFound("Could not find an alive cell!?");
        return best;
    }

    /// <summary>
    /// Returns the distance in steps to the nearest gold tile.
    /// </summary>
    /// <returns></returns>
    public int DistenceToNearestGold()
    {
        int best = int.MaxValue;
        foreach (VirtualCell cell in parentBoard.GetGolds())
        {
            int v = CubeCord.StepsBetween(cell.point.toCubeCord(), point.toCubeCord());
            if (v < best)
            {
                best = v;
            }
            //Debug.Log("Dist from " + point.ToString() + " to " + cell.point.ToString() + " = " + v + "\n" + point.toCubeCord().ToString() + " to " + cell.point.toCubeCord().ToString());
        }
        if (best == int.MaxValue) throw new CellNotFound("Could not find an alive cell!?");
        return best;
    }

    public override string ToString()
    {
        return point.ToString() + "  " + string.Format("Alive: {0} Team: {1}", alive, team);
    }
}

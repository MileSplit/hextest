﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// A small hack class to control the movement of the AI "hand"
/// </summary>
public class AiHand : MonoBehaviour {
    public static AiHand instance;
    public float speed = 1;
    public bool isExe = false;
    private Vector3 home;
    private List<Move> moveQue = new List<Move>();
    bool inited;
    private Move lastMove;

    private void Awake()
    {
        instance = this;
        home = transform.position;
    }

    public void ExeTurn(Turn turn)
    {
        isExe = true;
        inited = false;
        moveQue = turn.moves;
        ExeMove();
    }

    public void ExeMove()
    {
        print(inited);
        if (moveQue.Count > 0)
        {
            Vector3 pos = Board.GetCell(moveQue[0].position).transform.position;
            float dist = Vector3.Distance(gameObject.transform.position, pos);
            LeanTween.move(gameObject, pos, dist / speed).setDelay(0.5f).setOnComplete(ExeMove);
            if (inited)
                Board.GetCell(lastMove.position).OnMouseDown();
            lastMove = moveQue[0];
            inited = true;
            moveQue.RemoveAt(0);//Remove from que
        }
        else
        {
            if (inited)
                Board.GetCell(lastMove.position).OnMouseDown();
            float dist = Vector3.Distance(gameObject.transform.position, home);
            LeanTween.move(gameObject, home, dist / speed).setDelay(0.5f);
            isExe = false;
            Invoke("GoNextTurn", 1.5f);
        }
    }

    void GoNextTurn()
    {
        Board.NextTurn();
    }
}

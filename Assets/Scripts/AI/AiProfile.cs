﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
/// <summary>
/// A data class that holds the parameters of the AI logic.
/// </summary>
public class AiProfile : ScriptableObject {
    public string title = "Unnamed Leader";
    public int creationWeight = 200,
               agressionWeight = 400,
               maxNearGold = 15,
               goldValue = 100;
}

public class CreateAIProfile
{
    [MenuItem("Assets/Create/My Script-able Object")]
    public static void CreateMyAsset()
    {
        AiProfile asset = ScriptableObject.CreateInstance<AiProfile>();

        AssetDatabase.CreateAsset(asset, "Assets/NewScripableObject.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}

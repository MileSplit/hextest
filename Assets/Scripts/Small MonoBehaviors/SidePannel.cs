﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SidePannel : MonoBehaviour {

    Image image;


    private void Start()
    {
        image = GetComponent<Image>();
        Board_OnNextTurn();
    }
    // Use this for initialization
    void OnEnable()
    {
        Board.OnNextTurn += Board_OnNextTurn;
    }

    void OnDisable()
    {
        Board.OnNextTurn -= Board_OnNextTurn;
    }

    private void Board_OnNextTurn()
    {
        image.color = (Board.whosTurn == Team.P1) ? Board.P1Color : Board.P2Color;
    }
}

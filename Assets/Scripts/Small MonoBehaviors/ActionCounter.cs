﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ActionCounter : MonoBehaviour {

	void OnEnable () {
        Board.OnActionPerformed += Board_OnActionPerformed;
        Board.OnNextTurn += Board_OnActionPerformed;
	}
    private void OnDisable()
    {
        Board.OnActionPerformed -= Board_OnActionPerformed;
        Board.OnNextTurn -= Board_OnActionPerformed;
    }

    private void Board_OnActionPerformed()
    {
        GetComponent<Text>().text = "Moves\n" + (-Board.actionsThisTurn + Board.maxActions);
    }
}

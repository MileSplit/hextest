﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class EndTurnButton : MonoBehaviour {
    public Button thisButton;
    public Team team;


    private void Start()
    {
        thisButton = GetComponent<Button>();
        Board_OnNextTurn();
    }    
	// Use this for initialization
	void OnEnable () {
        Board.OnNextTurn += Board_OnNextTurn;
	}

    void OnDisable()
    {
        Board.OnNextTurn -= Board_OnNextTurn;
    }

    private void Board_OnNextTurn()
    {
        thisButton.interactable = (Board.whosTurn == team);
    }
}

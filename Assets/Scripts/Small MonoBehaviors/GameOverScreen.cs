﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScreen : MonoBehaviour {
    GameObject background;
    Text GameOverText;

    private void Awake()
    {
        background = transform.GetChild(0).gameObject;
        GameOverText = background.GetComponentInChildren<Text>();
        background.SetActive(false);
    }

    private void OnEnable()
    {
        Board.OnActionPerformed += Board_OnActionPerformed;
    }

    private void OnDisable()
    {
        Board.OnActionPerformed -= Board_OnActionPerformed;
    }

    private void Board_OnActionPerformed()
    {
        if (Board.GetTeam(Team.P1).Count <= 0)
        {
            background.SetActive(true);
            GameOverText.text = "Player 2 Wins!";
        }
        else if (Board.GetTeam(Team.P2).Count <= 0)
        {
            background.SetActive(true);
            GameOverText.text = "Player 1 Wins!";
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
/// <summary>
/// A small class to help generate a hexagon diagram.
/// </summary>
public class OctoGraph : MonoBehaviour {
    public Image ePercent;
    public static OctoGraph instance;

    public void Awake()
    {
        instance = this;
        gameObject.SetActive(false);
    }

    public void Show()
    {
        gameObject.SetActive(!gameObject.activeInHierarchy);
    }

    public void Render(float evalPercent)
    {
         ePercent.fillAmount = evalPercent;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Represents one hexagonal Cell in a board.
/// </summary>
public class Cell : MonoBehaviour {
    //Should be inited when genreated
    public GridPoint point = new GridPoint(0,0);
    private bool alive = false;
    public bool Alive
    {
        get
        {
            return alive;
        }
        set
        {
            alive = value;
            if (value == false) team = Team.Unclaimed;
            UpdateApperance();
        }
    }
    private Team team = Team.Unclaimed;
    public Team Team
    {
        get { return team; }
        set
        {
            team = value;
            UpdateApperance();
        }
    }
    public bool isOddRow
    {
        get
        {
            return !(point.x % 2 == 0);
        }
    }
    public bool GoldSpot
    {
        get
        {
            return goldSpotObj.gameObject.activeSelf;
        }
        set
        {
            goldSpotObj.gameObject.SetActive(value);
        }
    }
    private Transform goldSpotObj;


    private void OnEnable()
    {
        goldSpotObj = transform.GetChild(1);
        goldSpotObj.gameObject.SetActive(false);
    }

    private void UpdateApperance()
    {
        if (alive)
        {
            HexColor = team == Team.P1 ? Board.P1Color : Board.P2Color;
            CenterColor = Color.white;
        }
        else
        {
            HexColor = Color.black;
            CenterColor = Color.black;
        }
    }

    /// <summary>
    /// When using life and death this is used as a temp for the data.
    /// </summary>
    public FutureCell futureState;

    public SpriteRenderer sRenderer,cRenderer;

    /// <summary>
    /// Because i cant have normal constructors.
    /// </summary>
    public void Init(int x, int y)
    {
        sRenderer = GetComponent<SpriteRenderer>();
        cRenderer = transform.GetChild(0).GetComponent<SpriteRenderer>();
        point = new GridPoint(x, y);
        Board.AssignCell(this);
        gameObject.name = string.Format("Cell ({0},{1})", x, y);
        Alive = false;
    }

    public Color HexColor
    {
        get
        {
            return sRenderer.color;
        }
        set
        {
            if (value != sRenderer.color)
            {
                LeanTween.color(gameObject, value, 0.5f);
            }
        }
    }

    public Color CenterColor
    {
        get
        {
            return cRenderer.color;
        }
        set
        {
            if (value != cRenderer.color)
            {
                LeanTween.color(cRenderer.gameObject, value, 0.5f);
            }
        }
    }

    /// <summary>
    /// Gets the cell in a certain direction.
    /// </summary>
    public Cell GetCell(Direction direction)
    {
        GridPoint change;
        int oddChange = 0;
        if (isOddRow) oddChange = 1;
        switch (direction)
        {
            case Direction.N:
                change = new GridPoint(0,1, true);
                break;
            case Direction.NE:
                change = new GridPoint(1, 0 + oddChange, true);
                break;
            case Direction.SE:
                change = new GridPoint(1, -1 + oddChange, true);
                break;
            case Direction.S:
                change = new GridPoint(0, -1, true);
                break;
            case Direction.SW:
                change = new GridPoint(-1, -1 + oddChange, true);
                break;
            case Direction.NW:
                change = new GridPoint(-1, 0 + oddChange, true);
                break;
            default:
                change = new GridPoint(0,0, true);
                break;
        }
        try
        {
            return Board.GetCell(point + change);
        }
        catch (PointOutOfRange e)
        {
            throw new CellNotFound("Cell was not found, (Probaly out of range?)");
        }
        catch (NullReferenceException e)
        {
            throw new CellNotFound("Cell was not found, (Probaly a odd tile?)");
        }
    }

    public int CountNeighbors(Team team)
    {
        int counter = 0;
        foreach (Direction d in Enum.GetValues(typeof(Direction)))
        {
            try
            {
                Cell c = this.GetCell(d);
                if (c.Alive && c.Team == team) counter++;
            }
            catch (CellNotFound e)
            {
                //DO Nothing
            }
        }
        return counter;
    }

    /// <summary>
    /// Applys the temp state
    /// </summary>
    public void ApplyFutureState()
    {
        Alive = futureState.alive;
        Team = futureState.team;
    }

    public void OnMouseDown()
    {
        if (TestGen.instance.toggle.isOn)
        {
            print(string.Format("GridLoc: {0}  :  CubeLoc: {1}",point.ToString(), point.toCubeCord()));
            print(string.Format("Is alive: {0}, Team: {1}",Alive,Team));
            return;
        }
        if (GoldSpot) return;
        if (Board.actionsThisTurn < Board.maxActions)
        {
            Alive = !Alive;
            Team = Board.whosTurn;
            Board.actionsThisTurn++;
            Board.CallAction();
        }
    }
}

/// <summary>
/// A small helper class that shows what a cell will be like after a Gol.
/// </summary>
[System.Serializable]
public struct FutureCell
{
    public Team team;
    public bool alive;

    public FutureCell(Team team, bool alive)
    {
        this.team = team;
        this.alive = alive;
    }
}
/// <summary>
/// Thrown when a cell cannot be found from one method or another.
/// </summary>
public class CellNotFound : Exception
{
    public CellNotFound(string message) : base(message)
    {
    }
}
